/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab01ox;

import java.util.Scanner;

/**
 *
 * @author asus
 */
public class Lab01OX {

    public static void main(String[] args) {

        //กำหนดกระดานและสร้าง
        char[][] board = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '_';
            }
        }

        Scanner scan = new Scanner(System.in);
        System.out.println("- Welcome to OX games -");
        System.out.println("Start!!");
        printBoard(board);
        String player = "x";  //กำหนดให้ผู้เล่น x เล่นก่อน
        //boolean checkBoardFull = false;
        //boolean checkWinner = false;

        //System.out.print("Please Input row,col: ");
        //int row = scan.nextInt();
        //int col = scan.nextInt();
        //System.out.println(row);
        //System.out.println(col);
        
        while (true) {
            if(checkWinner(board) == true){
                break;
            }
            System.out.println(player + "  Turn!!");
            System.out.print("Please input row,col: ");
            int row = scan.nextInt();
            int col = scan.nextInt();
            if (row <= 3 && col <= 3) {
                System.out.println("row = " + row);
                System.out.println("col = " + col);
                if (player.equals("x")) {
                    if (row == 1) {
                        switch (col) {
                            case 1:
                                if (board[0][0] == '_') {
                                    board[0][0] = 'x';
                                    player = "o";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                            case 2:
                                if (board[0][1] == '_') {
                                    board[0][1] = 'x';
                                    player = "o";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                            case 3:
                                if (board[0][2] == '_') {
                                    board[0][2] = 'x';
                                    player = "o";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                        }

                    } else if (row == 2) {
                        switch (col) {
                            case 1:
                                if (board[1][0] == '_') {
                                    board[1][0] = 'x';
                                    player = "o";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                            case 2:
                                if (board[1][1] == '_') {
                                    board[1][1] = 'x';
                                    player = "o";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                            case 3:
                                if (board[1][2] == '_') {
                                    board[1][2] = 'x';
                                    player = "o";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                        }
                    } else if (row == 3) {
                        switch (col) {
                            case 1:
                                if (board[2][0] == '_') {
                                    board[2][0] = 'x';
                                    player = "o";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                            case 2:
                                if (board[2][1] == '_') {
                                    board[2][1] = 'x';
                                    player = "o";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                            case 3:
                                if (board[2][2] == '_') {
                                    board[2][2] = 'x';
                                    player = "o";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                        }
                    }

                } else if (player.equals("o")) {
                    if (row == 1) {
                        switch (col) {
                            case 1:
                                if (board[0][0] == '_') {
                                    board[0][0] = 'o';
                                    player = "x";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                            case 2:
                                if (board[0][1] == '_') {
                                    board[0][1] = 'o';
                                    player = "x";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                            case 3:
                                if (board[0][2] == '_') {
                                    board[0][2] = 'o';
                                    player = "x";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                        }

                    } else if (row == 2) {
                        switch (col) {
                            case 1:
                                if (board[1][0] == '_') {
                                    board[1][0] = 'o';
                                    player = "x";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                            case 2:
                                if (board[1][1] == '_') {
                                    board[1][1] = 'o';
                                    player = "x";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                            case 3:
                                if (board[1][2] == '_') {
                                    board[1][2] = 'o';
                                    player = "x";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                        }
                    } else if (row == 3) {
                        switch (col) {
                            case 1:
                                if (board[2][0] == '_') {
                                    board[2][0] = 'o';
                                    player = "x";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                            case 2:
                                if (board[2][1] == '_') {
                                    board[2][1] = 'o';
                                    player = "x";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                            case 3:
                                if (board[2][2] == '_') {
                                    board[2][2] = 'o';
                                    player = "x";
                                    break;
                                } else {
                                    System.out.println("Please input another row,col this position is full !!!!");
                                    break;
                                }
                        }
                    }

                }
                printBoard(board);
            } else {
                System.out.println("Please Input row, col between(1-3)");
            }
            
        }
        

    }

    ///ตรวจคนชนะ
    public static boolean checkWinner(char[][] board) {
        if ((board[0][0] == 'x' && board[0][1] == 'x' && board[0][2] == 'x' ) || //แนวนอนx
           (board[1][0] == 'x' && board[1][1] == 'x' && board[1][2] == 'x' ) ||
           (board[2][0] == 'x' && board[2][1] == 'x' && board[2][2] == 'x' ) ||
           
           (board[0][0] == 'x' && board[1][0] == 'x' && board[2][0] == 'x' ) || //แนวตั้งx
           (board[0][1] == 'x' && board[1][1] == 'x' && board[2][1] == 'x' ) ||
           (board[0][2] == 'x' && board[1][2] == 'x' && board[2][2] == 'x' ) ||
                
           (board[0][0] == 'x' && board[1][1] == 'x' && board[2][2] == 'x' ) ||
           (board[0][2] == 'x' && board[1][1] == 'x' && board[2][0] == 'x' ) ){
            System.out.println("=== Congreatulations ===");
            printBoard(board);
            System.out.println("The winner is X !!");
            System.out.println(">> END GAME <<");
            return true;
        }else if((board[0][0] == 'o' && board[0][1] == 'o' && board[0][2] == 'o' ) || //แนวนอนo
           (board[0][0] == 'o' && board[0][1] == 'o' && board[0][2] == 'o' ) ||
           (board[0][0] == 'o' && board[0][1] == 'o' && board[0][2] == 'o' ) ||
                
           (board[0][0] == 'o' && board[1][0] == 'o' && board[2][0] == 'o' ) || //แนวตั้งo
           (board[0][1] == 'o' && board[1][1] == 'o' && board[2][1] == 'o' ) ||
           (board[0][2] == 'o' && board[1][2] == 'o' && board[2][2] == 'o' ) ||

           (board[0][0] == 'o' && board[1][1] == 'o' && board[2][2] == 'o' ) ||
           (board[0][2] == 'o' && board[1][1] == 'o' && board[2][0] == 'o' ) ){
            System.out.println("=== Congreatulations ===");
            printBoard(board);
            System.out.println("The winner is O !!");
            System.out.println(">> END GAME <<");
            return true;
        }
        for (int i = 0; i< board.length;i++){     //เช็คว่าในตารางยังมีที่ว่างให้ลงไหม
            for(int j = 0; j < board[i].length; j++){
                if (board[i][j] =='_'){
                    return false;
                }
            }
        }
        printBoard(board);   
        System.out.println("The games ended in a tie !!!");   //นอกเหนือกรณีอื่นๆคือ เสมอกัน
        System.out.println(">> END GAME <<");
        return true;
    }

    
    
    ///ห้ามลบ ฟังก์ชันสร้างตาราง
    public static void printBoard(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print("|" + board[i][j] + "|");
            }
            System.out.println();
        }
    }

}